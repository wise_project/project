import pygame
import random
import time
import sys

# Initialize Pygame
pygame.init()

# Set up display
WIDTH, HEIGHT = 1550, 800
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Memory Puzzle Game")

# Colors
WHITE = (100, 200, 200)
BLACK = (0, 0, 0)
GRAY = (200, 255, 255)
PINK = (255, 105, 180)
RED = (255, 0, 0)



# Game variables
NUM_TILES = 4
TILE_SIZE = 120
MARGIN = 50
DELAY = 1  # Delay in seconds before tiles flip back

# Load images
images = []
# Load images
images = []
for i in range(NUM_TILES * NUM_TILES // 2):
    image = pygame.image.load(f"image{i}.jpg")  # Adjust the file extension if needed
    image = pygame.transform.scale(image, (TILE_SIZE, TILE_SIZE))
    images.append(image)

# Duplicate images for pairs
images *= 2
random.shuffle(images)

# Define tile class
class Tile:
    def _init_(self, image, pos):
        self.image = image
        self.pos = pos
        self.rect = pygame.Rect(pos[0], pos[1], TILE_SIZE, TILE_SIZE)
        self.visible = False

    def draw(self):
        if self.visible:
            WIN.blit(self.image, self.pos)
        else:
            pygame.draw.rect(WIN, GRAY, self.rect)

# Function to display welcome screen
def display_welcome_screen():
    WIN.fill(WHITE)
    welcome_image = pygame.image.load("welcome_image.png")
    welcome_image = pygame.transform.scale(welcome_image, (WIDTH, HEIGHT))
    WIN.blit(welcome_image, (0, 0))
    font = pygame.font.Font(None, 48)
    text = font.render("Welcome Gamer!",True, RED)
    text_rect = text.get_rect(center=(WIDTH // 2, HEIGHT // 2 - 250))
    WIN.blit(text, text_rect)
    instructions = font.render("Click to Start the Game", True, RED)
    instructions_rect = instructions.get_rect(center=(WIDTH // 2, HEIGHT // 2 + 250))
    WIN.blit(instructions, instructions_rect)
    pygame.display.flip()
    # Wait for user to click to start the game
    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                waiting = False

# Function to create tiles with images
def create_tiles():
    tiles = []
    total_width = NUM_TILES * TILE_SIZE + (NUM_TILES - 1) * MARGIN
    total_height = NUM_TILES * TILE_SIZE + (NUM_TILES - 1) * MARGIN
    start_x = (WIDTH - total_width) // 2
    start_y = (HEIGHT - total_height) // 2
    for i in range(NUM_TILES):
        for j in range(NUM_TILES):
            x = start_x + j * (TILE_SIZE + MARGIN)
            y = start_y + i * (TILE_SIZE + MARGIN)
            tile = Tile(images[i * NUM_TILES + j], (x, y))
            tiles.append(tile)
    return tiles

# Function to load the highest score from the file
def load_highest_score():
    try:
        with open("highest_score.txt", "r") as file:
            return float(file.read())
    except FileNotFoundError:
        return float('inf')
    except Exception as e:
        print("Error loading highest score:", e)
        return float('inf')

# Function to save the highest score
def save_highest_score(time_taken):
    try:
        with open("highest_score.txt", "w") as file:
            file.write(str(time_taken))
    except Exception as e:
        print("Error saving highest score:", e)

# Main game function
def main():
    # Display welcome screen
    display_welcome_screen()

    # Load music and create a music object
    try:
        music = pygame.mixer.Sound("background_music.mp3")  # Replace "background_music.mp3" with your music file
    except pygame.error as e:
        print("Error loading background music:", e)
        music = None

    # Create tiles
    tiles = create_tiles()

    # Load highest score
    best_time = load_highest_score()

    # Start playing music
    if music:
        music.play()

    # Main game loop
    flipped_tiles = []
    matched_tiles = []
    running = True
    start_time = time.time()  # Start time

    while running:
        # Calculate elapsed time
        elapsed_time = round(time.time() - start_time, 2)

        # Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if len(flipped_tiles) < 2:
                    mouse_pos = pygame.mouse.get_pos()
                    for tile in tiles:
                        if tile.rect.collidepoint(mouse_pos):
                            if tile not in flipped_tiles and tile not in matched_tiles:
                                tile.visible = True
                                flipped_tiles.append(tile)

        # Check if two tiles are flipped
        if len(flipped_tiles) == 2:
            pygame.display.flip()
            pygame.time.delay(DELAY * 1000)

            if flipped_tiles[0].image == flipped_tiles[1].image:
                matched_tiles.extend(flipped_tiles)
                flipped_tiles = []
            else:
                for tile in flipped_tiles:
                    tile.visible = False
                flipped_tiles = []

        # Draw tiles
        WIN.fill(WHITE)
        for tile in tiles:
            tile.draw()

        # Display scoreboard with elapsed time and best time
        display_scoreboard(elapsed_time, best_time)

        pygame.display.flip()

        # Check if all tiles are matched
        if len(matched_tiles) == len(tiles):
            font = pygame.font.Font(None, 36)
            if elapsed_time < best_time:
                text = font.render("Congratulations! New Best Time!", True, BLACK)
                text_rect = text.get_rect(center=(WIDTH // 2, HEIGHT // 1.1))
                WIN.blit(text, text_rect)
                save_highest_score(elapsed_time)  # Save the new best time
            else:
                text = font.render("Congratulations! You Won!", True, BLACK)
                text_rect = text.get_rect(center=(WIDTH // 2, HEIGHT // 1.1))
                WIN.blit(text, text_rect)
            pygame.display.flip()
            time.sleep(2)
            running = False

    # Stop playing music when the game ends
    if music:
        music.stop()

    return "game_over"


# Function to display quit and play again buttons
def display_end_screen():
    WIN.fill(WHITE)
    font = pygame.font.Font(None, 40)
    play_again_text = font.render("PLAY AGAIN", True, RED)
    play_again_rect = play_again_text.get_rect(center=(WIDTH // 2, HEIGHT // 2 - 50))
    WIN.blit(play_again_text, play_again_rect)
    quit_text = font.render("QUIT GAME", True, RED)
    quit_rect = quit_text.get_rect(center=(WIDTH // 2, HEIGHT // 2 + 45))
    WIN.blit(quit_text, quit_rect)
    pygame.display.flip()

    return play_again_rect,quit_rect
# Function to display the scoreboard with elapsed time and best time
# Function to display the scoreboard with elapsed time and best time
def display_scoreboard(elapsed_time, best_time):
    font = pygame.font.Font(None, 24)
    elapsed_text = font.render(f"Time: {elapsed_time} seconds", True, BLACK)
    elapsed_rect = elapsed_text.get_rect(topleft=(10, 10))
    WIN.blit(elapsed_text, elapsed_rect)
    if best_time != float('inf'):
        best_text = font.render(f"Best Time: {best_time} seconds", True, BLACK)
        best_rect = best_text.get_rect(topleft=(10, 40))
        WIN.blit(best_text, best_rect)
    pygame.display.flip()



# Function to handle button clicks in the end screen
def handle_end_screen_clicks(play_again_rect, quit_rect):
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                if play_again_rect.collidepoint(mouse_pos):
                    return "play_again"
                elif quit_rect.collidepoint(mouse_pos):
                    pygame.quit()
                    sys.exit()

# Main function
def main_loop():
    while True:
        result = main()
        if result == "game_over":
            play_again_rect, quit_rect = display_end_screen()
            action = handle_end_screen_clicks(play_again_rect, quit_rect)
            if action == "play_again":
                continue
            else:
                break

if _name_ == "_main_":
    main_loop()